package com.shadebyte.goldenknifes;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Core extends JavaPlugin {

	private static Core instance;

	@Override
	public void onEnable() {
		instance = this;
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
		Bukkit.getPluginManager().registerEvents(new KnifeEvents(), this);
		getCommand("knife").setExecutor(new KnifeCMD());
	}

	@Override
	public void onDisable() {
		instance = null;
	}

	public static Core getInstance() {
		return instance;
	}

	public ItemStack getGoldenKnife() {
		String rawItem = getConfig().getString("knife.item");
		String item[] = rawItem.split(":");
		ItemStack is = new ItemStack(Material.valueOf(item[0]), 1, (short) Short.parseShort(item[1]));
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', getConfig().getString("knife.name")));
		ArrayList<String> lore = new ArrayList<>();
		for (String all : getConfig().getStringList("knife.lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', all));
		}
		meta.setLore(lore);
		is.setItemMeta(meta);
		return is;
	}

	public boolean isInteger(String number) {
		try {
			Integer.parseInt(number);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
}
