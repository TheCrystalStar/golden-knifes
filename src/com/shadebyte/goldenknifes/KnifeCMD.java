package com.shadebyte.goldenknifes;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KnifeCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {

		if (!sender.hasPermission("goldenknife.cmd")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &cYou lack the required permision to use that command!"));
			return true;
		}

		if (args.length == 0) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&lGolen &e&lKnife"));
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/knife give <player> <amount>"));
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/knife giveall <amount>"));
			return true;
		}

		if (args.length == 1) {
			switch (args[0].toLowerCase()) {
			case "give":
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/knife give &c<player> <amount>"));
				break;
			case "giveall":
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/knife giveall &c<amount>"));
				break;
			}
			return true;
		}

		if (args.length == 2) {
			switch (args[0].toLowerCase()) {
			case "give":
				Player target = Bukkit.getPlayer(args[1]);
				if (target == null) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThat player is currently offline!"));
					return true;
				}
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/knife give " + args[1] + " &c<amount>"));
				break;
			case "giveall":
				if (!Core.getInstance().isInteger(args[1])) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ePlease enter a valid whole number."));
					return true;
				}

				for (int i = 0; i < Integer.parseInt(args[1]); i++) {
					Bukkit.getOnlinePlayers().forEach(all -> all.getInventory().addItem(Core.getInstance().getGoldenKnife()));
				}

				for (String text : Core.getInstance().getConfig().getStringList("messages.giveall.receiver")) {
					Bukkit.getOnlinePlayers().forEach(all -> all.sendMessage(ChatColor.translateAlternateColorCodes('&', text.replace("{amount}", args[1]))));
				}

				for (String text : Core.getInstance().getConfig().getStringList("messages.giveall.sender")) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', text.replace("{amount}", args[1])));
				}
				break;
			}
			return true;
		}

		if (args.length == 2) {
			switch (args[0].toLowerCase()) {
			case "give":
				Player target = Bukkit.getPlayer(args[1]);
				if (target == null) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThat player is currently offline!"));
					return true;
				}
				
				if (!Core.getInstance().isInteger(args[2])) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ePlease enter a valid whole number."));
					return true;
				}
				
				for (int i = 0; i < Integer.parseInt(args[2]); i++) {
					target.getInventory().addItem(Core.getInstance().getGoldenKnife());
				}

				for (String text : Core.getInstance().getConfig().getStringList("messages.give.receiver")) {
					Bukkit.getOnlinePlayers().forEach(all -> all.sendMessage(ChatColor.translateAlternateColorCodes('&', text.replace("{player}", args[1]).replace("{amount}", args[1]))));
				}

				for (String text : Core.getInstance().getConfig().getStringList("messages.give.sender")) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', text.replace("{amount}", args[1])));
				}
				break;
			}
			return true;
		}

		return true;
	}
}
