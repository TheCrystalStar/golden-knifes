package com.shadebyte.goldenknifes;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class KnifeEvents implements Listener {

	@EventHandler
	public void onPlayerChat(PlayerInteractAtEntityEvent e) {
		if (!(e.getRightClicked() instanceof Player)) {
			return;
		}

		Player p = e.getPlayer();
		Player target = (Player) e.getRightClicked();

		if (!p.hasPermission("goldenknife.use")) {
			return;
		}

		if (Bukkit.getPluginManager().getPlugin("WorldGuard") != null) {
			ApplicableRegionSet set = WGBukkit.getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation());
			for (ProtectedRegion region : set) {
				for (String allowed : Core.getInstance().getConfig().getStringList("blocked-worldguard-regions")) {
					if (region.getId().toLowerCase().equals(allowed.toLowerCase())) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cCannot use Golden Knifes in this region!"));
						return;
					}
				}
			}
			
			ApplicableRegionSet set2 = WGBukkit.getRegionManager(target.getWorld()).getApplicableRegions(target.getLocation());
			for (ProtectedRegion region : set2) {
				for (String allowed : Core.getInstance().getConfig().getStringList("blocked-worldguard-regions")) {
					if (region.getId().toLowerCase().equals(allowed.toLowerCase())) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cCannot use Golden Knifes in this region!"));
						return;
					}
				}
			}
		}

		target.setHealth(0D);

		if (p.getItemInHand().getAmount() >= 2) {
			p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
			p.updateInventory();
		} else {
			p.setItemInHand(null);
			p.updateInventory();
		}

		for (String msg : Core.getInstance().getConfig().getStringList("messages.knife-destroyed")) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
		}
	}
}
